package com.company;

import javax.print.DocFlavor.STRING;

public class Kereta {
    private String namaKereta;
    private Ticket tiketKAJJ, tiketKomuter;
    private int a = 0;
    private int b = 1001;
    private int jT1, jT2;
    private String asal [] = new String [1500];
    private String tujuan [] = new String [1500];
    private String nama [] = new String [1500];

    public Kereta(){
        this.jT1 = 1000;
    }

    public Kereta(String namaKereta, int jT2){
        this.namaKereta = namaKereta;
        this.jT2 = jT2;
    }
    public void tambahTiket(String nama){
        this.nama[a] = nama;
        a++;
        jT1--;

        if(jT1!=0 && jT1<1000){
        
            if(jT1<=30){
                System.out.println("===============================================");
                System.out.println("Tiket berhasil dipesan. Tiket tersisa "+ jT1);
            }else{
                System.out.println("===============================================");
                System.out.println("Tiket berhasil dipesan.");
            }
        }
    }
    public void tambahTiket(String nama, String asal, String tujuan){
        this.nama[b] = nama;
        this.asal[b] = asal;
        this.tujuan[b] = tujuan;
        b++;
        if(jT2!=0){
            jT2--;

            if(jT2<=30){
                System.out.println("===============================================");
                System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: "+jT2);
            }else{
                System.out.println("===============================================");
                System.out.println("Tiket berhasil dipesan.");
            }
        }else if(jT2<=0){
            System.out.println("===============================================");
            System.out.println("Tiket telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }
    public void tampilkanTiket(){
        if(namaKereta==null){
            System.out.println("===============================================");
            System.out.println("Daftar nama penumpang KA Kommuter:");
            System.out.println("===============================================");
            for (int i = 0; i < a; i++) {
                System.out.println("Nama   : "+nama[i]);
            }
        }else if(namaKereta!=null){
            System.out.println("===============================================");
            System.out.println("Daftar penumpang KA "+ namaKereta);
            for (int i = 1001; i < b; i++) {
                System.out.println("--------------------------------------");
                System.out.println("Nama   : "+nama[i]);
                System.out.println("Asal   : "+asal[i]);
                System.out.println("Tujuan : "+tujuan[i]);
            }
            System.out.println("--------------------------------------");
        }
    }

}
